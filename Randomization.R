#DO NOT RUN. Re-does all the randomizations.

#outfiles
# FT_randomization = 'Randomized_genotypes_FT.csv'
GE_randomization = 'Randomized_genotypes_GE.csv'
# FT_plots = 'Genotype_map_FT.pdf'
GE_plots = 'Genotype_map_GE.pdf'

FT_randomization = 'foo1.csv'
GE_randomization = 'foo2.csv'
FT_plots = 'foo3.pdf'
GE_plots = 'foo4.pdf'

#load genotypes
library(gdata)
genotypes = read.xls('Final genotype list.xlsx',sheet=3,h=T,stringsAsFactors=F)[1:25,1]


#draw tray
draw_tray = function(rows,cols,text=NULL,colors = NULL,main=NULL,cex.text = 0.8){
	nr = length(rows)
	nc = length(cols)
	if(is.null(colors)){
		colors = matrix(0,nr=nr,ncol=nc)
	}
	if(is.null(text)){
		text = matrix('',nr=nr,ncol=nc)
	}
	width = height = 1/max(nr+1,nc+1)
	plot(NA,NA,xlim=c(0,1),ylim=c(0,1),main=main,xaxt='n',yaxt='n',xlab='',ylab='')
	for(i in 1:nr){
		for(j in 1:nc){
			rect((j)*width,1-(i+1)*height,(j+1)*width,1-(i)*height,col=colors[i,j])
			text((j+0.5)*width,1-(i+0.5)*height,labels=text[i,j],adj=c(0.5,0.5),cex=cex.text)
		}
	}
	for(i in 1:nr){
		j = 0
		text((j+0.5)*width,1-(i+0.5)*height,labels=rows[i],adj=c(0.5,0.5))
	}
	for(j in 1:nc){
		i = 0
		text((j+0.5)*width,1-(i+0.5)*height,labels=cols[j],adj=c(0.5,0.5))
	}
}

#Define experiment
Treatments = c('Con', 'Var')
Blocks = LETTERS[1:2]  #Two blocks per treatment
trays_per_block = 3 #3 trays per block
tray_info = data.frame(Tray = 1:(trays_per_block*length(Treatments)*length(Blocks)),
						Treatment = rep(Treatments,each = trays_per_block*length(Blocks)),
						Block = rep(rep(Blocks,each = trays_per_block),length(Treatments)
						))


Row = LETTERS[1:5]
Col = 1:10
plants_per_genotype_per_tray = 2

data = data.frame(Treatment = '',Block = '', Tray = '', Row = '', Col = '', Genotype = '',stringsAsFactors=F)[-1,]
for(i in 1:dim(tray_info)[1]){
	data = rbind(data,data.frame(
		Treatment = tray_info$Treatment[i],
		Block = tray_info$Block[i],
		Tray = tray_info$Tray[i],
		Row = rep(Row,each = length(Col)),
		Col = rep(Col,length(Row)),
		Genotype = sample(rep(genotypes,each = plants_per_genotype_per_tray),replace=F),
		stringsAsFactors=F))
}
data$TrayID = paste(data$Tray,"_",data$Treatment,data$Block,sep="")

pdf(FT_plots,height = 6,width=10)
par(bty='n')
par(mar=c(0,0,2,0))
for(tray in unique(data$TrayID)){
	i = data$TrayID == tray
	draw_tray(Row,Col,text = matrix(data$Genotype[i],nr=length(Row),byrow=T),main = tray)
}
dev.off()


write.table(data,file = FT_randomization,sep=',',row.names=F,col.names=T)



#now for gene expression tray:

#groups of 4. 
groups = list(
		paste(c('Col','ColFRI'),'T1_1',sep='::'),
		paste(c('Col','ColFRI'),'T1_2',sep='::'),
		paste(c('Col','ColFRI'),'T1_3',sep='::'),
		paste(c('Col','ColFRI'),'T1_4',sep='::'),
		paste(c('Col','ColFRI'),'T1_6',sep='::'),
		c('Col::T1_5','ColFRI::T2_6'),
		paste('ColFRI',c('T2_1','T3_6'),sep='::'),
		paste('ColFRI',c('T2_2','T3_1'),sep='::'),
		paste('ColFRI',c('T2_3','T3_2'),sep='::'),
		paste('ColFRI',c('T2_4','T3_3'),sep='::'),
		paste('ColFRI',c('T2_5','T3_4'),sep='::'),
		paste('ColFRI',c('T1_5','T3_5'),sep='::')
		)

Row = LETTERS[1:4]
Col = 1:12

tray_num = 12
data = data.frame(Treatment = '',Block = '', Tray = '', Row = '', Col = '', Sample = '',stringsAsFactors=F)[-1,]
for(trt in Treatments){
	for(blk in Blocks){
		tray_num = tray_num+1
		sort_groups = groups[sample(1:length(groups))]
		group_num = 0
		for(i in 1:2){
			for(j in 1:6){
				group_num = group_num + 1
				data = rbind(data,data.frame(
					Treatment = trt,
					Block = blk,
					Tray = tray_num,
					Row = Row[2*i-c(1,1,0,0)],
					Col = Col[2*j-c(1,0,1,0)],
					Sample = sample(rep(sort_groups[[group_num]],each=2)),
					stringsAsFactors=F))
			}
		}
	}
}
data$TrayID = paste(data$Tray,"_",data$Treatment,data$Block,sep="")
pdf(GE_plots,height = 6,width=10)
par(bty='n')
par(mar=c(0,0,2,0))
for(tray in unique(data$TrayID)){
	ind = data$TrayID == tray
	X = matrix('',length(Row),length(Col))
	for(i in 1:dim(X)[1]){
		for(j in 1:dim(X)[2]){
			X[i,j] = data$Sample[ind][data$Row[ind] == Row[i] & data$Col[ind]==Col[j]]
		}
	}
	Y = array(0,dim=dim(X))
	Y[sapply(X,function(x) strsplit(x,'::',fixed=t)[[1]][1] == 'Col')] = 'grey70'
	draw_tray(Row,Col,text = X,main = tray,cex.text = .6,colors=Y)
}
dev.off()


write.table(data,file = GE_randomization,sep=',',row.names=F,col.names=T)

